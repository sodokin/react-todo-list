import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import TodoForm from './components/Form';

function App() {
  return (
    <div className="App">
      <h1 className='text-center mt-3'>Todo-List</h1>
      <TodoForm/>
    </div>
  );
}

export default App;
